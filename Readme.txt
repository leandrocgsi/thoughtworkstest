A quest�o escolhida para essa implementa��o foi a n�3 "MERCHANT'S GUIDE TO THE GALAXY"
======================================================================================

COMO RODAR O PROJETO
=====================================================
A solu��o foi implementada na linguagem Java e como ferramenta de build foi usado o Maven. 

Segui os princ�pios do TDD para desenvolver o projeto e pra isso foi usado a biblioteca de testes JUnit.

Para testar o projeto basta importar em sua IDE favorita que suporte Maven e realizar o build. Feito isso basta executar a classe Main
como uma aplica��o Java.

A seguinte mensagem ser� lan�ada na tela "Starting intergalactic transactions assistant! Please type your instructions."

A partir de ent�o basta digitar linha a linha pressionando enter ou colar o trecho abaixo.

O sistema inicia o processamento ao se pressionar enter duas vezes.


	glob is I
	prok is V
	pish is X
	tegj is L
	glob glob Silver is 34 Credits
	glob prok Gold is 57800 Credits
	pish pish Iron is 3910 Credits
	how much is pish tegj glob glob ?
	how many Credits is glob prok Silver ?
	how many Credits is glob prok Gold ?
	how many Credits is glob prok Iron ?
	how much wood could a woodchuck chuck if a woodchuck could chuck wood ?

o sistema ir� imprimir os resultados esperados logo abaixo.

Al�m disso existem testes unit�rios cobrindo uma parcela razo�vel do c�digo.


COMO FOI IMPLEMENTADO
=====================================================

O projeto foi implementado seguindo os preceitos do TDD onde as principais classes foram desenvolvidas ap�s a implementa��o dos testes. Levou-se em
conta tamb�m o princ�pio de alta coes�o e baixo acoplamento, visando uma maior facilidade para manter bem como testar o c�digo. Na medida do poss�vel 
procurou-se manter cada classe com apenas uma responsabilidade. A estrutura b�sica da aplica��o est� organizada conforme a descri��o abaixo.

src
	main
		java
			br.com.thoughtworks                       - Aqui est� a classe Main respons�vel por inicializar a aplica��o
			br.com.thoughtworks.configuration         - Aqui est� a classe respons�vel por armazenar as configura��es da aplica��o como os
														valores dos n�meros romanos na liguagem intergal�ctica bem como dos cr�ditos.
			br.com.thoughtworks.converter             - Aqui temos a classe RomanToInteger respons�vel por converter um numero romano como
														XIX em inteiro 9.
			br.com.thoughtworks.converter.enumeration - Aqui temos os enums Romans que armazena o valor em romano e em ar�bico dos 7 valores base e o
														enum LineType que lista os tipos de linhas existentes na aplica��o. 
			br.com.thoughtworks.converter.filter      - Aqui temos a classe FilterLineTypes que � respons�vel por separar os 4 tipos de linha que
														podem ser processadas pelo sistema
			br.com.thoughtworks.input                 - Aqui temos as classes InputReader e LineProcessor respons�veis respectivamente por ler os
														inputs digitados no console e por rotear o processamento de cada tipo de linha.
			br.com.thoughtworks.output                - Aqui temos a classe OutputWritter que � a respons�vel por escrever o resultado do
														processamento dos dados digitados.
			br.com.thoughtworks.processors            - Aqui temos as classes HowManyCreditsQuery, HowMuchQuery, InputConfig e InputCreditConfig que
														s�o as respons�veis por processar as informa��es de cada uma das 4 linhas.
			br.com.thoughtworks.processors.output     - Aqui temos a classe Output que armazena os resultados do processamento at� a impress�o ser
														realizada.
			br.com.thoughtworks.processors.utils      - Aqui temos a classe Utils com os utilit�rios da aplica��o.
	test
		java
			br.com.thoughtworks.converter             - Aqui temos a classe de teste RomanToIntegerTest respons�vel por testar os m�todos da
														classe RomanToInteger.
			br.com.thoughtworks.converter.filter      - Aqui temos a classe de teste FilterLineTypesTest respons�vel por testar os m�todos da
														classe FilterLineTypes.
			br.com.thoughtworks.processors            - Aqui temos as classes HowManyCreditsQueryTest, HowMuchQueryTest, InputConfigTest e
														InputCreditConfigTest respons�veis por testar as classes HowManyCreditsQuery,
														HowMuchQuery, InputConfig e InputCreditConfig. 