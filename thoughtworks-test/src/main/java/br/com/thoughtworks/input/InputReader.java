package br.com.thoughtworks.input;

import java.util.Scanner;

public class InputReader {
    
    private LineProcessor lineProcessor = new LineProcessor();
    private Scanner scan = new Scanner(System.in);

    public void read() {
        String line;

        while (this.scan.hasNextLine() && (line = this.scan.nextLine()).length() > 0) {
            lineProcessor.processLine(line);
        }
    }
}