package br.com.thoughtworks.input;

import br.com.thoughtworks.converter.filter.FilterLineTypes;
import br.com.thoughtworks.converter.enumeration.LineType;
import br.com.thoughtworks.processors.HowManyCreditsQuery;
import br.com.thoughtworks.processors.HowMuchQuery;
import br.com.thoughtworks.processors.InputConfig;
import br.com.thoughtworks.processors.InputCreditConfig;
import br.com.thoughtworks.processors.output.Output;

public class LineProcessor {

    FilterLineTypes filterLineTypes = new FilterLineTypes();

    InputConfig inputConfig = new InputConfig();
    InputCreditConfig inputCreditConfig = new InputCreditConfig();
    HowManyCreditsQuery howManyCreditsQuery = new HowManyCreditsQuery();
    HowMuchQuery howMuchQuery = new HowMuchQuery();

    public void processLine(String line) {
        
        final LineType lineType = filterLineTypes.getLineType(line);
        
        if (lineType.equals(LineType.INPUT_CONFIG)) {
            inputConfig.processInputConfigLine(line);
        } else if (lineType.equals(LineType.INPUT_CREDIT_CONFIG)) {
            inputCreditConfig.processCreditConfigLine(line);
        } else if (lineType.equals(LineType.HOW_MUCH_QUERY)) {
            howMuchQuery.processHowMuchQuery(line);
        } else if (lineType.equals(LineType.HOW_MANY_QUERY)) {
            howManyCreditsQuery.processHowManyCreditsQuery(line);
        } else {
            Output.output.add("I have no idea what you are talking about");
        }
    }
}
