package br.com.thoughtworks.converter.enumeration;

public enum LineType {
    INPUT_CONFIG, 
    INPUT_CREDIT_CONFIG, 
    HOW_MUCH_QUERY, 
    HOW_MANY_QUERY, 
    WTF_ARE_YOU_SAYING
}