package br.com.thoughtworks.converter.filter;

import br.com.thoughtworks.converter.enumeration.LineType;

public class FilterLineTypes {

    public static String inputConfigPattern = "^([A-Za-z]+) is ([I|V|X|L|C|D|M])$";
    public static String inputCreditConfigPattern = "^([A-Za-z]+)([A-Za-z\\s]*) is ([0-9]+) ([c|C]redits)$";
    public static String queryHowMuchPattern = "^how much is (([A-Za-z\\s])+)\\?$";
    public static String queryHowManyPattern = "^how many [c|C]redits is (([A-Za-z\\s])+)\\?$";

    public LineType getLineType(String line) {
        line = line.trim();

        if (line.matches(inputConfigPattern)) {
            return LineType.INPUT_CONFIG;
        } else if (line.matches(inputCreditConfigPattern)) {
            return LineType.INPUT_CREDIT_CONFIG;
        } else if (line.matches(queryHowMuchPattern)) {
            return LineType.HOW_MUCH_QUERY;
        } else if (line.matches(queryHowManyPattern)) {
            return LineType.HOW_MANY_QUERY;
        }
        return LineType.WTF_ARE_YOU_SAYING;
    }
}
