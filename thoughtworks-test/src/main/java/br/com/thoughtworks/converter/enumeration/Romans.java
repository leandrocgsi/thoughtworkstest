package br.com.thoughtworks.converter.enumeration;

public enum Romans {

    I(1),
    V(5),
    X(10),
    L(50),
    C(100),
    D(500),
    M(1000);
    int arabicNumber;

    Romans(int value) {
        arabicNumber = value;
    }

    public int getArabicNumber() {
        return arabicNumber;
    }
}