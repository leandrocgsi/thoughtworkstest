package br.com.thoughtworks.converter;

import br.com.thoughtworks.converter.enumeration.Romans;

public class RomanToInteger {

    private Romans romanToNumber;

    public Integer romanToIntegerConverter(String romanNumberString) {
        int romanCharsNumbers = 0, arabicNumber = 0, romanNumberLenght = romanNumberString.length();
        int[] arrayRomansArabics = new int[romanNumberLenght];
        arrayRomansArabics = getArrayRomansArabics(romanNumberLenght, romanNumberString, romanCharsNumbers, arrayRomansArabics);
        arabicNumber = getArabicNumber(romanNumberLenght, arrayRomansArabics, arabicNumber);
        int lastNumberOfArray = arrayRomansArabics[romanNumberLenght - 1];
        return addValueOfCurrentRomanCharToArabicNumber(arabicNumber, lastNumberOfArray);
    }

    private int[] getArrayRomansArabics(int romanNumberLenght, String romanNumberString, int romanCharsNumbers, int[] arrayRomansArabics) {
        for (int i = 0; i < romanNumberLenght; i++) {
            String romanChar = romanNumberString.substring(romanCharsNumbers, ++romanCharsNumbers);
            romanToNumber = Romans.valueOf(romanChar);
            arrayRomansArabics[i] = romanToNumber.getArabicNumber();
        }
        return arrayRomansArabics;
    }

    private int getArabicNumber(int romanNumberLenght, int[] arrayRomansArabics, int arabicNumber) {
        for (int j = 0; j < (romanNumberLenght - 1); j++) {
            
            int currentNumber = arrayRomansArabics[j];
            int nextNumber = arrayRomansArabics[j + 1];

            if (currentNumberIsGreatherThanNextNumber(currentNumber, nextNumber)) {
                arabicNumber = addValueOfCurrentRomanCharToArabicNumber(arabicNumber, currentNumber);
            } else {
                arabicNumber = subtractValueOfCurrentRomanCharToArabicNumber(arabicNumber, currentNumber);
            }
        }
        return arabicNumber;
    }

    private static int subtractValueOfCurrentRomanCharToArabicNumber(int arabicNumber, int currentNumber) {
        return arabicNumber -= currentNumber;
    }

    private static int addValueOfCurrentRomanCharToArabicNumber(int arabicNumber, int currentNumber) {
        return arabicNumber += currentNumber;
    }

    private boolean currentNumberIsGreatherThanNextNumber(int currentNumber, int nextNumber) {
        return currentNumber >= nextNumber;
    }
}