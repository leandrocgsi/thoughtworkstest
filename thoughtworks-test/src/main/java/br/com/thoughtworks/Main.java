package br.com.thoughtworks;

import br.com.thoughtworks.input.InputReader;
import br.com.thoughtworks.output.OutputWritter;

public class Main extends Thread {

    public static void main(String[] args) {
        
        InputReader inputReader = new InputReader();

        System.out.println("Starting intergalactic transactions assistant! Please type your instructions.");
        inputReader.read();
        OutputWritter.write();
        System.out.println("Intergalactic transactions assistant has stopped");
    }
}