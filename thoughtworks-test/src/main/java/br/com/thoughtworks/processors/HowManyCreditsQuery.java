package br.com.thoughtworks.processors;

import br.com.thoughtworks.configuration.ConvertersSettings;
import br.com.thoughtworks.converter.RomanToInteger;
import br.com.thoughtworks.processors.output.Output;
import br.com.thoughtworks.processors.utils.Utils;
import java.util.Stack;

public class HowManyCreditsQuery {

    RomanToInteger romanToInteger = new RomanToInteger();

    public void processHowManyCreditsQuery(String line) {

        try {
            String formatted = line.split("(\\sis\\s)")[1];
            formatted = Utils.removeQuestionSignal(formatted);

            String[] keys = formatted.split("\\s");

            boolean found = false;
            int romanNumber = 0;
            String roman = "";
            String outputResult = null;
            Stack<Float> creditValues = new Stack<Float>();

            for (String key : keys) {
                found = false;

                String romanValue = ConvertersSettings.constantConfigurations.get(key);
                if (romanValue != null) {
                    roman += romanValue;
                    found = true;
                }

                String computedValue = ConvertersSettings.calculatedConfigurations.get(key);
                if (!found && computedValue != null) {
                    creditValues.push(Float.parseFloat(computedValue));
                    found = true;
                }

                if (!found) {
                    System.out.println("I have no idea what you are talking about!");
                    break;
                }
            }

            processResults(found, creditValues, roman, romanNumber, outputResult, formatted);

        } catch (Exception e) {
            System.out.println("Please input a how many credits query line type " + e.getMessage());
        }
    }

    private void processResults(boolean found, Stack<Float> creditValues, String roman, int romanNumber, String outputResult, String formatted) {
        if (found) {
            float creditsResult = 1;
            for (int i = 0; i < creditValues.size(); i++) {
                creditsResult *= creditValues.get(i);
            }
            
            int finalResult = (int) creditsResult;
            if (roman.length() > 0) {
                romanNumber = romanToInteger.romanToIntegerConverter(roman);
            }
            finalResult = (int) (romanNumber * creditsResult);
            outputResult = formatted + " is " + finalResult + " Credits";
        }
        
        Output.output.add(outputResult);
    }
}
