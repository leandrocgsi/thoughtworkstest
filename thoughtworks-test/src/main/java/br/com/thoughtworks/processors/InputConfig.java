package br.com.thoughtworks.processors;

import br.com.thoughtworks.configuration.ConvertersSettings;

public class InputConfig {

    public void processInputConfigLine(String line) {

        String[] splitedLine = line.trim().split("\\s+");

        try {
            final String key = splitedLine[0];
            final String romanValue = splitedLine[2];
            ConvertersSettings.constantConfigurations.put(key, romanValue);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Please input a correct line type " + e.getMessage());
        }
    }
}
