package br.com.thoughtworks.processors.utils;

public class Utils {
    
    public static String removeQuestionSignal(String formatted) {
        return formatted.replace("?","").trim();
    }
    
    public static String formatInputString(String line, String pattern) {
        return line.replaceAll(pattern, "").trim();
    }
    
}
