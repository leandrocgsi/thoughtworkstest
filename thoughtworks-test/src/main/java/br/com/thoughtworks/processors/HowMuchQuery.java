package br.com.thoughtworks.processors;

import br.com.thoughtworks.configuration.ConvertersSettings;
import br.com.thoughtworks.converter.RomanToInteger;
import br.com.thoughtworks.processors.output.Output;
import br.com.thoughtworks.processors.utils.Utils;

public class HowMuchQuery {

    public void processHowMuchQuery(String line) {
        
        RomanToInteger romanToInteger = new RomanToInteger();
        
        try {
            String formatted = line.split("\\sis\\s")[1].trim();
            formatted = Utils.removeQuestionSignal(formatted);

            String keys[] = formatted.split("\\s+");

            String arabicValue = "";
            String resultMessage = null;
            boolean errorOccured = false;

            for (String key : keys) {
                String romanValue = ConvertersSettings.constantConfigurations.get(key);
                if (romanValue == null) {
                    System.out.println("I have no idea what you are talking about!");
                    errorOccured = true;
                    break;
                }
                arabicValue += romanValue;
            }

            if (!errorOccured) {
                arabicValue = (romanToInteger.romanToIntegerConverter(arabicValue)).toString();
                resultMessage = formatted + " is " + arabicValue;
            }

            Output.output.add(resultMessage);

        } catch (Exception e) {
            System.out.println("Please input a how much query line type " + e.getMessage());
        }
    }
}
