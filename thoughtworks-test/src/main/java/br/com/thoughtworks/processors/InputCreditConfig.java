package br.com.thoughtworks.processors;

import br.com.thoughtworks.configuration.ConvertersSettings;
import br.com.thoughtworks.converter.RomanToInteger;
import br.com.thoughtworks.processors.utils.Utils;

public class InputCreditConfig {

    RomanToInteger romanToInteger = new RomanToInteger();
    
    public void processCreditConfigLine(String line) {
        try {
            String formatted = Utils.formatInputString(line, "(is\\s+)|([c|C]redits\\s*)");
            String[] keys = getKeyArray(formatted);
            String creditsType = keys[keys.length - 2];
            float creditsValue = getCreditValue(keys);
            String romanCharacter = processRomanNumber(keys);
            int romanValue = romanToInteger.romanToIntegerConverter(romanCharacter);
            float credit = processCreditValue(creditsValue, romanValue);
            ConvertersSettings.calculatedConfigurations.put(creditsType, credit + "");
        } catch (Exception e) {
            System.out.println("Please input a correct credit line type " + e.getMessage());
        }
    }

    private static String[] getKeyArray(String formatted) {
        return formatted.split("\\s");
    }

    private static float getCreditValue(String[] keys) throws NumberFormatException {
        return Float.parseFloat(keys[keys.length - 1]);
    }

    private String processRomanNumber(String[] keys) {
        String romanCharacter = "";
        for (int i = 0; i < keys.length - 2; i++) {
            romanCharacter += ConvertersSettings.constantConfigurations.get(keys[i]);
        }
        return romanCharacter;
    }

    private static float processCreditValue(float creditsValue, int romanValue) {
        return (float) (creditsValue / romanValue);
    }
}