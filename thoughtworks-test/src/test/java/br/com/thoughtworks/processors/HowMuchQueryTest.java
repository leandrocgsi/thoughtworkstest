package br.com.thoughtworks.processors;

import br.com.thoughtworks.processors.output.Output;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class HowMuchQueryTest {

    InputCreditConfig inputCreditConfig = new InputCreditConfig();
    InputConfig inputConfig = new InputConfig();
    
    HowMuchQuery howMuchQuery = new HowMuchQuery();
    
    @Test
    public void inputHowMuchQueryTeste() {
        
        inputConfig.processInputConfigLine("glob is I");
        inputConfig.processInputConfigLine("prok is V");
        inputConfig.processInputConfigLine("pish is X");
        inputConfig.processInputConfigLine("tegj is L");
        
        inputCreditConfig.processCreditConfigLine("glob glob Silver is 34 Credits");
        inputCreditConfig.processCreditConfigLine("glob prok Gold is 57800 Credits");
        inputCreditConfig.processCreditConfigLine("pish pish Iron is 3910 Credits");
        
        howMuchQuery.processHowMuchQuery("how much is pish tegj glob glob ?");
                
        assertTrue(Output.output.contains("pish tegj glob glob is 42"));
    }
}
