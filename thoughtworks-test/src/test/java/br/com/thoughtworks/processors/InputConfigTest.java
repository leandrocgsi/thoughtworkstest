package br.com.thoughtworks.processors;

import br.com.thoughtworks.configuration.ConvertersSettings;
import static org.junit.Assert.assertTrue;
import org.junit.Test;


public class InputConfigTest {

    InputConfig inputConfig = new InputConfig();
    
    @Test
    public void inputConfigsTeste() {
        inputConfig.processInputConfigLine("glob is I");
        inputConfig.processInputConfigLine("prok is V");
        inputConfig.processInputConfigLine("pish is X");
        inputConfig.processInputConfigLine("tegj is L");
        inputConfig.processInputConfigLine("pato is C");
        inputConfig.processInputConfigLine("miner is D");
        
        assertTrue(ConvertersSettings.constantConfigurations.get("glob").equals("I"));
        assertTrue(ConvertersSettings.constantConfigurations.get("prok").equals("V"));
        assertTrue(ConvertersSettings.constantConfigurations.get("pish").equals("X"));
        assertTrue(ConvertersSettings.constantConfigurations.get("tegj").equals("L"));
        assertTrue(ConvertersSettings.constantConfigurations.get("pato").equals("C"));
        assertTrue(ConvertersSettings.constantConfigurations.get("miner").equals("D"));
        
    }
}