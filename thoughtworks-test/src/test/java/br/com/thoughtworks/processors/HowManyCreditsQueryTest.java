package br.com.thoughtworks.processors;

import br.com.thoughtworks.processors.output.Output;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class HowManyCreditsQueryTest {

    InputCreditConfig inputCreditConfig = new InputCreditConfig();
    InputConfig inputConfig = new InputConfig();
    
    HowManyCreditsQuery howManyCreditsQuery = new HowManyCreditsQuery();
    
    @Test
    public void inputHowManyCreditsQueryTeste() {
        
        inputConfig.processInputConfigLine("glob is I");
        inputConfig.processInputConfigLine("prok is V");
        inputConfig.processInputConfigLine("pish is X");
        inputConfig.processInputConfigLine("tegj is L");
        
        inputCreditConfig.processCreditConfigLine("glob glob Silver is 34 Credits");
        inputCreditConfig.processCreditConfigLine("glob prok Gold is 57800 Credits");
        inputCreditConfig.processCreditConfigLine("pish pish Iron is 3910 Credits");
        
        howManyCreditsQuery.processHowManyCreditsQuery("how many Credits is glob prok Silver ?");
        howManyCreditsQuery.processHowManyCreditsQuery("how many Credits is glob prok Gold ?");
        howManyCreditsQuery.processHowManyCreditsQuery("how many Credits is glob prok Iron ?");
                
        assertTrue(Output.output.contains("glob prok Silver is 68 Credits"));
        assertTrue(Output.output.contains("glob prok Gold is 57800 Credits"));
        assertTrue(Output.output.contains("glob prok Iron is 782 Credits"));
    }
}
