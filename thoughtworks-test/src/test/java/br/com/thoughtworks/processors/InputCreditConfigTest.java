package br.com.thoughtworks.processors;

import br.com.thoughtworks.configuration.ConvertersSettings;
import static org.junit.Assert.assertTrue;
import org.junit.Test;


public class InputCreditConfigTest {

    InputCreditConfig inputCreditConfig = new InputCreditConfig();
    InputConfig inputConfig = new InputConfig();
    
    @Test
    public void inputCreditConfigsTeste() {
        
        inputConfig.processInputConfigLine("glob is I");
        inputConfig.processInputConfigLine("prok is V");
        inputConfig.processInputConfigLine("pish is X");
        inputConfig.processInputConfigLine("tegj is L");
        
        inputCreditConfig.processCreditConfigLine("glob glob Silver is 34 Credits");
        inputCreditConfig.processCreditConfigLine("glob prok Gold is 57800 Credits");
        inputCreditConfig.processCreditConfigLine("pish pish Iron is 3910 Credits");
        inputCreditConfig.processCreditConfigLine("glob Dust is 1 Credits");
        
        assertTrue(ConvertersSettings.calculatedConfigurations.get("Silver").equals("17.0"));
        assertTrue(ConvertersSettings.calculatedConfigurations.get("Gold").equals("14450.0"));
        assertTrue(ConvertersSettings.calculatedConfigurations.get("Iron").equals("195.5"));
        assertTrue(ConvertersSettings.calculatedConfigurations.get("Dust").equals("1.0"));
    }
}