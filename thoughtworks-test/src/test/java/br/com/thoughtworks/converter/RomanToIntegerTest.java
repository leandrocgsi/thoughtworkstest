package br.com.thoughtworks.converter;

import static org.junit.Assert.*;

import org.junit.Test;

public class RomanToIntegerTest {

    RomanToInteger romanToInteger = new RomanToInteger();

    @Test
    public void convertOne() {
        assertTrue(romanToInteger.romanToIntegerConverter("I") == 1);
    }

    @Test
    public void convertFive() {
        assertTrue(romanToInteger.romanToIntegerConverter("V") == 5);
    }

    @Test
    public void convertTen() {
        assertTrue(romanToInteger.romanToIntegerConverter("X") == 10);
    }

    @Test
    public void convertFifTeen() {
        assertTrue(romanToInteger.romanToIntegerConverter("L") == 50);
    }

    @Test
    public void convertOneHundred() {
        assertTrue(romanToInteger.romanToIntegerConverter("C") == 100);
    }

    @Test
    public void convertFiveHundred() {
        assertTrue(romanToInteger.romanToIntegerConverter("D") == 500);
    }

    @Test
    public void convertOneThousand() {
        assertTrue(romanToInteger.romanToIntegerConverter("M") == 1000);
    }

    @Test
    public void convertFour() {
        assertTrue(romanToInteger.romanToIntegerConverter("IV") == 4);
    }

    @Test
    public void convertFortyThree() {
        assertTrue(romanToInteger.romanToIntegerConverter("XLIII") == 43);
    }

    @Test
    public void thousandNineHundredFortyThree() {
        assertTrue(romanToInteger.romanToIntegerConverter("MCMXLIII") == 1943);
    }

    @Test
    public void twoThousandNineHundredFortyThree() {
        assertTrue(romanToInteger.romanToIntegerConverter("MMCMXLIII") == 2943);
    }

    @Test
    public void thousandSixHundredNinetyTwo() {
        assertTrue(romanToInteger.romanToIntegerConverter("MDCXCII") == 1692);
    }

    @Test
    public void thousandFiveHundred() {
        assertTrue(romanToInteger.romanToIntegerConverter("MD") == 1500);
    }

    @Test
    public void thousandNineHundredEightyFour() {
        assertTrue(romanToInteger.romanToIntegerConverter("MCMLXXXIV") == 1984);
    }
}
