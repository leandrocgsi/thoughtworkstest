package br.com.thoughtworks.converter.filter;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class FilterLineTypesTest {
  	FilterLineTypes filterLineTypes = new FilterLineTypes();
	
	@Test
	public void inputConfigLineITest() {
           assertTrue((filterLineTypes.getLineType("glob is I")).toString().equals("INPUT_CONFIG"));
           System.out.println("O TIPO É " + filterLineTypes.getLineType("glob is I"));
	} 
        
	@Test
	public void inputConfigLineVTest() {
           assertTrue((filterLineTypes.getLineType("prok is V")).toString().equals("INPUT_CONFIG"));
           System.out.println("O TIPO É " + filterLineTypes.getLineType("prok is V"));
	}  
        
	@Test
	public void inputConfigLineXTest() {
           assertTrue((filterLineTypes.getLineType("pish is X")).toString().equals("INPUT_CONFIG"));
           System.out.println("O TIPO É " + filterLineTypes.getLineType("pish is X"));
	}
        
	@Test
	public void inputConfigLineLTest() {
           assertTrue((filterLineTypes.getLineType("tegj is L")).toString().equals("INPUT_CONFIG"));
           System.out.println("O TIPO É " + filterLineTypes.getLineType("tegj is L"));
	}  
                
	@Test
	public void inputConfigLineSilverTest() {
           assertTrue((filterLineTypes.getLineType("glob glob Silver is 34 Credits")).toString().equals("INPUT_CREDIT_CONFIG"));
           System.out.println("O TIPO É " + filterLineTypes.getLineType("glob glob Silver is 34 Credits"));
	}  
        
	@Test
	public void inputConfigLineGoldTest() {
           assertTrue((filterLineTypes.getLineType("glob prok Gold is 57800 Credits")).toString().equals("INPUT_CREDIT_CONFIG"));
           System.out.println("O TIPO É " + filterLineTypes.getLineType("glob prok Gold is 57800 Credits"));
	}
        
	@Test
	public void inputConfigLineIronTest() {
           assertTrue((filterLineTypes.getLineType("pish pish Iron is 3910 Credits")).toString().equals("INPUT_CREDIT_CONFIG"));
           System.out.println("O TIPO É " + filterLineTypes.getLineType("pish pish Iron is 3910 Credits"));
	}  
                
	@Test
	public void inputHowMuchQueryLineTest() {
           assertTrue((filterLineTypes.getLineType("how much is pish tegj glob glob ?")).toString().equals("HOW_MUCH_QUERY"));
           System.out.println("O TIPO É " + filterLineTypes.getLineType("how much is pish tegj glob glob ?"));
	}
        
	@Test
	public void inputIHaveNoIdeaLineTest() {
           assertTrue((filterLineTypes.getLineType("how much wood could a woodchuck chuck if a woodchuck could chuck wood ?")).toString().equals("WTF_ARE_YOU_SAYING"));
           System.out.println("O TIPO É " + filterLineTypes.getLineType("how much wood could a woodchuck chuck if a woodchuck could chuck wood ?"));
	}  
        
	@Test
	public void inputHowManySilverQueryLineTest() {
           assertTrue((filterLineTypes.getLineType("how many Credits is glob prok Silver ?")).toString().equals("HOW_MANY_QUERY"));
           System.out.println("O TIPO É " + filterLineTypes.getLineType("how many Credits is glob prok Silver ?"));
	}
                        
	@Test
	public void inputHowManyGoldQueryLineTest() {
           assertTrue((filterLineTypes.getLineType("how many Credits is glob prok Gold ?")).toString().equals("HOW_MANY_QUERY"));
           System.out.println("O TIPO É " + filterLineTypes.getLineType("how many Credits is glob prok Gold ?"));
	}                
	@Test
	public void inputHowManyIronQueryLineTest() {
           assertTrue((filterLineTypes.getLineType("how many Credits is glob prok Iron ?")).toString().equals("HOW_MANY_QUERY"));
           System.out.println("O TIPO É " + filterLineTypes.getLineType("how many Credits is glob prok Iron ?"));
	}
}